import React, { Component } from 'react';
import officeMap from './assets/office-map-38.png';
import './App.css';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      count: 90,
      density: [
        {
          clientCount: 1,
          ID: 0
        },
        {
          clientCount: 1,
          ID: 1
        },
        {
          clientCount: 1,
          ID: 2
        },
        {
          clientCount: 1,
          ID: 3
        },
        {
          clientCount: 1,
          ID: 4
        },
        {
          clientCount: 1,
          ID: 5
        },
    ]
    };
    setInterval(this.updateData, 5000);
  }

  updateData = () => {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() { 
      // eslint-disable-next-line
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
          this.setState({density:  this.normalize(JSON.parse(xmlHttp.response))});
          console.log(this.state.density)
        }
    }.bind(this)
    xmlHttp.open("GET", "http://3.87.19.238:3000/get-all", true);
    xmlHttp.setRequestHeader('Content-Type', 'text/plain'); 
    xmlHttp.send(null);
  }

  normalize = (density) => {
    let max = Math.max(...density.map(density => density.clientCount))
    let min = Math.min(...density.map(density => density.clientCount))
    return density.map(density => ({...density, clientCount: this.normal(density.clientCount, max, min)}))
  }
  
  normal = (val, max, min) => { 
    return (val - min) / (max - min); 
  }

  getQuadrantDensityColor = (quadrant) => {
    let signalCount = this.state.density.find((density) => quadrant === density.ID).clientCount;
    var h = (1.0 - signalCount) * 240
    return {backgroundColor: "hsla(" + h + ", 100%, 50%, .3)"}
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
        <div className="map-container">
          <img src={officeMap} className="App-logo" alt="logo" />
          <div className="map-overlay">
            <div className="map-overlay-tile" style={this.getQuadrantDensityColor(1)}>NE</div>
            <div className="map-overlay-tile" style={this.getQuadrantDensityColor(2)}>NW</div>
            <div className="map-overlay-tile" style={this.getQuadrantDensityColor(3)}>SE</div>
            <div className="map-overlay-tile" style={this.getQuadrantDensityColor(4)}>SW</div>
          </div>
        </div>
          <p>
            View the office heatmap!
          </p>
        </header>
      </div>
    );
  }
}

export default App;
